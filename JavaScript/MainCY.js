var busqueda = 5;
var arbol = new ArbolCY();

arbol.nodoPadre.hI = arbol.agregarNodo(arbol.nodoPadre,"Hoja Izquierda","16",16);
arbol.nodoPadre.hD = arbol.agregarNodo(arbol.nodoPadre,"Hoja Derecha","20",20);

//--Hijos del 16
arbol.nodoPadre.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI,"Hoja Izquierda","8",8);
arbol.nodoPadre.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hI,"Hoja Derecha","8",8);

arbol.nodoPadre.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hoja Izquierda","a",4);
arbol.nodoPadre.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hoja Derecha","4",4);

arbol.nodoPadre.hI.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD,"Hoja Izquierda","t",2);
arbol.nodoPadre.hI.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD,"Hoja Derecha","m",2);

arbol.nodoPadre.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hoja Izquierda","e",4);
arbol.nodoPadre.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hoja Derecha","4",4);

arbol.nodoPadre.hI.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD,"Hoja Izquierda","n",2);
arbol.nodoPadre.hI.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD,"Hoja Derecha","2",2);

arbol.nodoPadre.hI.hI.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD,"Hoja Izquierda","o",1);
arbol.nodoPadre.hI.hI.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD,"Hoja Derecha","u",1);

//--Hijos del 20
arbol.nodoPadre.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Izquierda","8",8);
arbol.nodoPadre.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Derecha","12",12);

arbol.nodoPadre.hD.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI,"Hoja Izquierda","4",4);
arbol.nodoPadre.hD.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI,"Hoja Derecha","4",4);

arbol.nodoPadre.hD.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI,"Hoja Izquierda","i",2);
arbol.nodoPadre.hD.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI,"Hoja Derecha","2",2);

arbol.nodoPadre.hD.hI.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD,"Hoja Izquierda","x",1);
arbol.nodoPadre.hD.hI.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD,"Hoja Derecha","p",1);

arbol.nodoPadre.hD.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD,"Hoja Izquierda","h",2);
arbol.nodoPadre.hD.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD,"Hoja Derecha","s",200);

arbol.nodoPadre.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD,"Hoja Izquierda","5",5);
arbol.nodoPadre.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD,"Hoja Derecha"," ",7);

arbol.nodoPadre.hD.hD.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hoja Izquierda","2",2);
arbol.nodoPadre.hD.hD.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hoja Derecha","f",3);

arbol.nodoPadre.hD.hD.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI,"Hoja Izquierda","r",1);
arbol.nodoPadre.hD.hD.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI,"Hoja Drecha","l",1);

//--Buscar nodos en nivel----------------
var resultado = arbol.verificarNivelHijos(arbol.nodoPadre);
//console.log(resultado);

//--Buscar Nodo--------------------------
var busqueda = arbol.buscarValor(busqueda,arbol.nodoPadre);
console.log(busqueda);

//--Buscar caminos-----------------------
var caminoNodo = arbol.buscarCaminoNodo(busqueda);
console.log("El camino a seguir es: "+caminoNodo);

//--Sumar camino-------------------------
var suma = arbol.sumarCaminoNodo(busqueda);
console.log("La suma es: "+suma);